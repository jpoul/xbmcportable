AutoItSetOption("TrayAutoPause", 0)
AutoItSetOption("TrayIconHide", 1)

Switch @OSVersion
Case "WIN_VISTA", "WIN_7", "WIN_2008", "WIN_2008R2"

	If FileExists (@UserProfileDir & "\AppData\Roaming\XBMC") Then
		ProgressOn("XBMC Portable", "", "Starting up, please wait...")
		DirMove (@UserProfileDir & "\AppData\Roaming\XBMC", @UserProfileDir & "\AppData\Roaming\XBMC_INSTALLED")
		DirCopy (@ScriptDir & "\Data", @UserProfileDir & "\AppData\Roaming\XBMC")
		ProgressOff ( )
		RunWait (@ScriptDir & "\App\XBMC\XBMC.exe")
		ProgressOn("XBMC Portable", "", "Cleaning up, please wait...")
		DirCopy (@UserProfileDir & "\AppData\Roaming\XBMC\", @ScriptDir & "\Data", 1)
		ProgressSet( 33 )
		DirRemove (@UserProfileDir & "\AppData\Roaming\XBMC", 1)
		ProgressSet( 66 )
		DirMove (@UserProfileDir & "\AppData\Roaming\XBMC_INSTALLED", @UserProfileDir & "\AppData\Roaming\XBMC")
		ProgressSet( 100 )
		sleep (2000)
		ProgressOff ( )
	Else
		ProgressOn("XBMC Portable", "", "Starting up, please wait...")
		DirCopy (@ScriptDir & "\Data", @UserProfileDir & "\AppData\Roaming\XBMC")
		ProgressSet ( 100 )
		ProgressOff ( )
		RunWait (@ScriptDir & "\App\XBMC\XBMC.exe")
		ProgressOn("XBMC Portable", "", "Cleaning up, please wait...")
		DirCopy (@UserProfileDir & "\AppData\Roaming\XBMC", @ScriptDir & "\Data", 1)
		ProgressSet( 50 )
		DirRemove (@UserProfileDir & "\AppData\Roaming\XBMC", 1)
		ProgressSet( 100 )
		sleep (2000)
		ProgressOff ( )
		EndIf

Case  "WIN_XP", "WIN_XPe", "WIN_2003"

	If FileExists (@UserProfileDir & "\Application Data\XBMC") Then
		ProgressOn("XBMC Portable", "", "Starting up, please wait...")
		DirMove (@UserProfileDir & "\Application Data\XBMC", @UserProfileDir & "\Application Data\XBMC_INSTALLED")
		DirCopy (@ScriptDir & "\Data", @UserProfileDir & "\Application Data\XBMC")
		ProgressOff ( )
		RunWait (@ScriptDir & "\App\XBMC\XBMC.exe")
		ProgressOn("XBMC Portable", "", "Cleaning up, please wait...")
		DirCopy (@UserProfileDir & "\Application Data\XBMC", @ScriptDir & "\Data", 1)
		ProgressSet( 33 )
		DirRemove (@UserProfileDir & "\Application Data\XBMC", 1)
		ProgressSet( 66 )
		DirMove (@UserProfileDir & "\Application Data\XBMC_INSTALLED", @UserProfileDir & "\Application Data\XBMC")
		ProgressSet( 100 )
		sleep (2000)
		ProgressOff ( )
	Else
		ProgressOn("XBMC Portable", "", "Starting up, please wait...")
		DirCopy (@ScriptDir & "\Data", @UserProfileDir & "\Application Data\XBMC")
		ProgressSet ( 100 )
		ProgressOff ( )
		RunWait (@ScriptDir & "\App\XBMC\XBMC.exe")
		ProgressOn("XBMC Portable", "", "Cleaning up, please wait...")
		DirCopy (@UserProfileDir & "\Application Data\XBMC", @ScriptDir & "\Data", 1)
		ProgressSet( 50 )
		DirRemove (@UserProfileDir & "\Application Data\XBMC", 1)
		ProgressSet( 100 )
		sleep (2000)
		ProgressOff ( )
		EndIf

Case Else
MsgBox(64, "Not Supported", "Sorry, your operating system is not supported.")
EndSwitch
Exit